#= require jquery
#= require jquery.ex-table-filter
#= require jquery_ujs
#= require turbolinks
#= require bootstrap
#= require handsontable/dist/handsontable.full
#= require_tree .

exports = this
exports.ht = null

onReady = ->
  $('form[class$="_subject"]').on 'submit', ->
    $('#subject_instructor_ids option, #subject_student_ids option').prop('selected', true)

$(document).ready onReady
$(document).on 'page:load', onReady

exports.renderHandsonTable = (tableData) ->
  new Handsontable($('#handsontable')[0], $.extend(tableData, {
    allowInsertColumn: false,
    allowInsertRow: false,
    afterChange: (changes, source) ->
      self = this

      console.log "changes = #{ changes }, source = #{ source }"

      return if /^(?:loadData|undo)$/.test(source)

      changes.forEach (change) ->
        data = id: self.getDataAtRowProp(change[0], 'id')
        data["#{ change[1] }"] = change[3]

        $.ajax
          url: "/results/#{ data.id }.json"
          type: 'PUT'
          data: 
            result: data
          error: ->
            self.setDataAtRowProp change[0], change[1], change[2], 'undo'
  }))

exports.addItems = (from, to) ->
  console.log "from = #{ from }\n to = #{ to }"

  from.find('option:selected').prop('selected', false).each (idx, opt) ->
    to.append opt

  opts = to.children 'option'
  opts.sort (a, b) -> a.value - b.value
  opts.detach().appendTo to
