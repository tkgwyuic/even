module ApplicationHelper
  def shallow_args parent, child
    child.try(:new_record?) ? [parent, child] : child
  end

  def current_semester
    today = Date.today
    semester = (4 ... 10).cover?(today.month) ? 'first_half' : 'second_half'

    Semester.find_by(year: today.year, semester: Semester.semesters[semester])
  end

  def fa_new_icon
    fa_icon 'plus', text: t(:new)
  end

  def fa_edit_icon
    fa_icon 'pencil-square-o', text: t(:edit)
  end

  def fa_delete_icon
    fa_icon 'trash-o', text: t(:delete)
  end

  def dropdown_menu(klass, items_path)
    render partial: 'layouts/dropdown', locals: {
      items: klass.includes(:instructors).includes(:semester).where(
        { 'instructors.id': current_user.id },
        { 'semester.year': current_semester.year }),
      items_path: items_path,
    }
  end
end
