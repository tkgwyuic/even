class GroupsStudent < ActiveRecord::Base
  default_scope { includes(:student).order('students.ruby') }

  belongs_to :group
  belongs_to :student

  validates :group, presence: true
  validates :student, presence: true
end
