class Semester < ActiveRecord::Base
  enum semester: {
    first_half: 1,
    second_half: 2,
  }

  default_scope { order 'year DESC, semester DESC' }

  has_many :groups, dependent: :destroy
  has_many :subjects, dependent: :destroy

  validates :year, presence: true, uniqueness: { scope: :semester }
  validates :semester, presence: true, inclusion: { in: self.semesters }

  def to_s
    I18n.t(:'semester.year', year: year) + ', ' + I18n.t(:"semester.semesters.#{ semester }")
  end

  def self.now
    t = Time.zone.now
    s = t.month.between?(4, 9) ? 'first_half' : 'second_half'

    self.find_or_create_by year: t.year, semester: semesters[s]
  end
end
