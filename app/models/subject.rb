class Subject < ActiveRecord::Base
  acts_as_paranoid

  belongs_to :semester
  validates :semester, presence: true

  belongs_to :base, class_name: 'SubjectBase'
  validates :base, presence: true

  has_and_belongs_to_many :instructors

  has_many :students_subjects, dependent: :destroy
  has_many :students, through: :students_subjects
  has_many :results, through: :students_subjects

  def name
    base.name
  end

  def name=(val)
    base.name = val
  end

  def unit
    base.unit
  end

  def time
    base.time
  end
end
