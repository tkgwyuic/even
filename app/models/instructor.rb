class Instructor < ActiveRecord::Base
  authenticates_with_sorcery!

  after_initialize :set_defaults

  has_and_belongs_to_many :groups
  has_and_belongs_to_many :subjects

  validates :code, presence: true, uniqueness: true
  validates :name, presence: true
  validates :perms, presence: true,
    numericality: { greater_than_or_equal_to: 0, only_integer: true }


  with_options if: :new_record? do |record|
    record.validates :password, confirmation: true, length: { minimum: 8 }
    record.validates :password_confirmation, presence: true
  end

  def admin?
    (perms & 1) != 0
  end

  def to_s
    name
  end

  private def set_defaults
    perms ||= 0
  end
end
