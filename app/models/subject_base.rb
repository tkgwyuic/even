class SubjectBase < ActiveRecord::Base
  validates :name, presence: true

  has_many :subjects, foreign_key: :base_id, dependent: :destroy
end
