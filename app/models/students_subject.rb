class StudentsSubject < ActiveRecord::Base
  after_create :create_result

  belongs_to :student
  validates :student, presence: true

  belongs_to :subject
  validates :subject, presence: true

  has_one :result, dependent: :destroy

  def to_s
    student.name
  end
end
