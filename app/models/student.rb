class Student < ActiveRecord::Base
  acts_as_paranoid

  has_and_belongs_to_many :groups, -> { uniq }

  has_many :students_subjects, dependent: :destroy
  has_many :subjects, through: :students_subjects
  has_many :results, through: :students_subjects

  validates :code, presence: true, uniqueness: { scope: :deleted_at }
  validates :name, presence: true
end
