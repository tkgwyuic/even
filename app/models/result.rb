class Result < ActiveRecord::Base
  enum status: ['takeable', 'passed', 'absented', 'stopped']

  default_scope { includes(:student).order('students.ruby, students.name') }

  after_initialize :set_defaults

  belongs_to :students_subject
  validates :students_subject, presence: true

  has_one :student, through: :students_subject
  has_one :subject, through: :students_subject

  validates :midterm, :termend, :resit, :final,
    allow_nil: true,
    numericality: {
      greater_than_or_equal_to: 0,
      less_than_or_equal_to: 100,
      only_integer: true,
    }

  validates :midterm_status, :termend_status, :resit_status,
    allow_nil: true, inclusion: { in: self.statuses.values }

  private def set_defaults
    status ||= 0
  end
end
