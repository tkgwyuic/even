class Group < ActiveRecord::Base
  acts_as_paranoid

  belongs_to :semester
  belongs_to :course

  has_and_belongs_to_many :instructors
  validates :instructors, presence: true

  has_many :groups_students, dependent: :destroy
  has_many :students, through: :groups_students
end
