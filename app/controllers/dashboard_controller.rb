class DashboardController < ApplicationController
  def index
    @groups = Group.includes(:instructors).where('instructors.id': current_user.id)
    @subjects = Subject.includes(:instructors).where('instructors.id': current_user.id)
  end
end
