class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  rescue_from CanCan::AccessDenied do |exception|
    # render file: "#{ Rails.root }/public/404.html", status: 403, layout: false
    redirect_back_or_to root_url, flash: { error: exception.message }
  end

  before_action :require_login
  before_action :set_semester

  private def not_authenticated
    redirect_to login_url, alert: 'First log in to view this page.'
  end

  private def set_semester
    @semester = Semester.now
  end
end
