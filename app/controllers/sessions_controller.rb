class SessionsController < ApplicationController
  skip_before_filter :require_login, except: [:destroy]

  layout false

  def new
    @user = Instructor.new
  end

  def create
    if @user = login(params[:code], params[:password])
      redirect_back_or_to root_url, notice: 'Login successful.'
    else
      redirect_to login_url, alert: 'Login failed.'
    end
  end

  def destroy
    logout
    redirect_to :login, notice: 'Logged out.'
  end
end
