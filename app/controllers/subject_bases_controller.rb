class SubjectBasesController < ApplicationController
  before_action :set_subject_basis, only: [:show, :edit, :update, :destroy]

  # GET /subject_bases
  # GET /subject_bases.json
  def index
    @subject_bases = SubjectBase.all
  end

  # GET /subject_bases/1
  # GET /subject_bases/1.json
  def show
  end

  # GET /subject_bases/new
  def new
    @subject_basis = SubjectBase.new
  end

  # GET /subject_bases/1/edit
  def edit
  end

  # POST /subject_bases
  # POST /subject_bases.json
  def create
    @subject_basis = SubjectBase.new(subject_basis_params)

    respond_to do |format|
      if @subject_basis.save
        format.html { redirect_to @subject_basis, notice: 'Subject base was successfully created.' }
        format.json { render :show, status: :created, location: @subject_basis }
      else
        format.html { render :new }
        format.json { render json: @subject_basis.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /subject_bases/1
  # PATCH/PUT /subject_bases/1.json
  def update
    respond_to do |format|
      if @subject_basis.update(subject_basis_params)
        format.html { redirect_to @subject_basis, notice: 'Subject base was successfully updated.' }
        format.json { render :show, status: :ok, location: @subject_basis }
      else
        format.html { render :edit }
        format.json { render json: @subject_basis.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /subject_bases/1
  # DELETE /subject_bases/1.json
  def destroy
    @subject_basis.destroy
    respond_to do |format|
      format.html { redirect_to subject_bases_url, notice: 'Subject base was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_subject_basis
      @subject_basis = SubjectBase.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def subject_basis_params
      params.require(:subject_basis).permit(:name, :unit, :time)
    end
end
