json.array!(@subject_bases) do |subject_basis|
  json.extract! subject_basis, :id, :name, :unit, :time
  json.url subject_basis_url(subject_basis, format: :json)
end
