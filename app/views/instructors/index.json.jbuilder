json.array!(@instructors) do |instructor|
  json.extract! instructor, :id, :code, :name
  json.url instructor_url(instructor, format: :json)
end
