json.array!(@groups) do |group|
  json.extract! group, :id, :semester, :course_id, :year, :name
  json.url group_url(group, format: :json)
end
