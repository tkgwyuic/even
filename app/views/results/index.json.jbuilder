json.array!(@results) do |result|
  json.extract! result, :id, :students_subject_id, :midterm, :termend, :resit, :final
  json.url subject_result_url(@subject, result, format: :json)
end
