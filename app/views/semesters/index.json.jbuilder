json.array!(@semesters) do |semester|
  json.extract! semester, :id, :year, :semester
  json.url semester_url(semester, format: :json)
end
