json.array!(@students) do |student|
  json.extract! student, :id, :code, :name
  json.url student_url(student, format: :json)
end
