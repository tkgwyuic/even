ENV['BUNDLE_GEMFILE'] ||= File.expand_path('../../Gemfile', __FILE__)

require 'bundler/setup' # Set up gems listed in the Gemfile.

if ENV['USER'][0,2].downcase == 'j4'
  require 'rails/commands/server'
  module Rails
    class Server
      def default_options
        super.merge({
          Host: '0.0.0.0',
          Port: (ENV['USER'][-2,2].to_i + 3000),
        })
      end
    end
  end
end
