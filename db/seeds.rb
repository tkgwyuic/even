# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
require 'gimei'

[
  { name: '情報処理科' },
  { name: '情報処理科３年制' },
  { name: '高度情報システム科' },
  { name: 'ウェブ・メディア科' },
].each do |params|
  Course.find_or_create_by(params)
end

[
  { name: 'Java演習Ⅰ', unit: 1.5, time: 2 },
  { name: 'Java演習Ⅱ', unit: 1.5, time: 2 },
  { name: '応用プログラミングⅠ', unit: 1.5, time: 2 },
  { name: 'アセンブラ', unit: 1.5, time: 2 },
  { name: '表計算', unit: 1.5, time: 2 },
  { name: 'ハードウェア', unit: 2, time: 1 },
  { name: 'ソフトウェア', unit: 2, time: 1 },
  { name: 'ネットワーク', unit: 2, time: 1 },
  { name: 'データベース', unit: 2, time: 1 },
  { name: 'セキュリティ', unit: 2, time: 1 },
].each do |params|
  SubjectBase.find_or_create_by(params)
end

1.upto(10) do |n|
  g = Gimei.new
  Instructor.create({
    code: 'TC%02d' % [n],
    name: g.kanji,
    ruby: g.hiragana,
    password: 'password',
    password_confirmation: 'password',
    perms: n & 1,
  })
end

2012.upto(2015).to_a.product(Semester.semesters.values) do |y, s|
  Semester.find_or_create_by({
    year: y,
    semester: s,
  })
end

14.upto(15).to_a.product(['AA', 'AB'], 1.upto(10).to_a) do |y, s, n|
  g = Gimei.new
  Student.find_or_create_by({
    code: '%02d%s%04d' % [y, s, n],
    name: g.kanji,
    ruby: g.hiragana,
  })
end

Group.create([
  {
    semester: Semester.find_by(year: 2014, semester: Semester.semesters['first_half']),
    course: Course.first,
    name: 'A1',
    grade: 1,
    instructors: Instructor.limit(1),
    students: Student.where('code LIKE ?', '14AA%'),
  },
  {
    semester: Semester.find_by(year: 2015, semester: Semester.semesters['first_half']),
    course: Course.first,
    name: 'A2',
    grade: 2,
    instructors: Instructor.limit(1),
    students: Student.where('code LIKE ?', '14AA%'),
  },
  {
    semester: Semester.find_by(year: 2015, semester: Semester.semesters['second_half']),
    course: Course.first,
    name: 'A1',
    grade: 1,
    instructors: Instructor.limit(3),
    students: Student.where('code LIKE ?', '15AA%'),
  },
])

Subject.create([
  {
    semester: Semester.find_by(year: 2014, semester: Semester.semesters['first_half']),
    base: SubjectBase.first,
    instructors: Instructor.limit(1),
    students: Student.limit(10),
  },
  {
    semester: Semester.find_by(year: 2015, semester: Semester.semesters['first_half']),
    base: SubjectBase.offset(1).first,
    instructors: Instructor.limit(3),
    students: Student.limit(20),
  },
  {
    semester: Semester.find_by(year: 2015, semester: Semester.semesters['second_half']),
    base: SubjectBase.offset(1).first,
    instructors: Instructor.limit(5),
    students: Student.offset(10).limit(10),
  },
])
