# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150610044154) do

  create_table "courses", force: :cascade do |t|
    t.string "name", null: false
  end

  create_table "groups", force: :cascade do |t|
    t.integer  "semester_id", null: false
    t.integer  "course_id",   null: false
    t.string   "name",        null: false
    t.integer  "grade",       null: false
    t.datetime "deleted_at"
  end

  add_index "groups", ["course_id"], name: "index_groups_on_course_id"
  add_index "groups", ["semester_id"], name: "index_groups_on_semester_id"

  create_table "groups_instructors", id: false, force: :cascade do |t|
    t.integer "group_id",      null: false
    t.integer "instructor_id", null: false
  end

  add_index "groups_instructors", ["group_id"], name: "index_groups_instructors_on_group_id"
  add_index "groups_instructors", ["instructor_id"], name: "index_groups_instructors_on_instructor_id"

  create_table "groups_students", force: :cascade do |t|
    t.integer "group_id"
    t.integer "student_id"
    t.string  "code"
  end

  add_index "groups_students", ["group_id"], name: "index_groups_students_on_group_id"
  add_index "groups_students", ["student_id"], name: "index_groups_students_on_student_id"

  create_table "instructors", force: :cascade do |t|
    t.string   "code",                         null: false
    t.string   "name",                         null: false
    t.string   "ruby",                         null: false
    t.string   "crypted_password",             null: false
    t.string   "salt",                         null: false
    t.integer  "perms",            default: 0, null: false
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  add_index "instructors", ["code"], name: "index_instructors_on_code", unique: true

  create_table "instructors_subjects", id: false, force: :cascade do |t|
    t.integer "instructor_id", null: false
    t.integer "subject_id",    null: false
  end

  create_table "results", force: :cascade do |t|
    t.integer  "students_subject_id",             null: false
    t.integer  "midterm"
    t.integer  "termend"
    t.integer  "resit"
    t.integer  "final"
    t.integer  "midterm_status",      default: 0
    t.integer  "termend_status",      default: 0
    t.integer  "resit_status",        default: 0
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.datetime "deleted_at"
  end

  add_index "results", ["students_subject_id"], name: "index_results_on_students_subject_id"

  create_table "semesters", force: :cascade do |t|
    t.integer "year",     null: false
    t.integer "semester", null: false
  end

  add_index "semesters", ["year", "semester"], name: "index_semesters_on_year_and_semester", unique: true

  create_table "students", force: :cascade do |t|
    t.string   "code",       null: false
    t.string   "name",       null: false
    t.string   "ruby"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
  end

  add_index "students", ["code", "deleted_at"], name: "index_students_on_code_and_deleted_at", unique: true

  create_table "students_subjects", force: :cascade do |t|
    t.integer "student_id"
    t.integer "subject_id"
    t.integer "result_id"
  end

  add_index "students_subjects", ["result_id"], name: "index_students_subjects_on_result_id"
  add_index "students_subjects", ["student_id"], name: "index_students_subjects_on_student_id"
  add_index "students_subjects", ["subject_id"], name: "index_students_subjects_on_subject_id"

  create_table "subject_bases", force: :cascade do |t|
    t.string  "name", null: false
    t.decimal "unit", null: false
    t.decimal "time", null: false
  end

  add_index "subject_bases", ["name"], name: "index_subject_bases_on_name", unique: true

  create_table "subjects", force: :cascade do |t|
    t.integer  "semester_id", null: false
    t.integer  "base_id",     null: false
    t.datetime "deleted_at"
  end

  add_index "subjects", ["base_id"], name: "index_subjects_on_base_id"
  add_index "subjects", ["semester_id"], name: "index_subjects_on_semester_id"

end
