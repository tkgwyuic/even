class CreateSubjectBases < ActiveRecord::Migration
  def change
    create_table :subject_bases do |t|
      t.string :name, null: false
      t.decimal :unit, null: false
      t.decimal :time, null: false
    end

    add_index :subject_bases, :name, unique: true
  end
end
