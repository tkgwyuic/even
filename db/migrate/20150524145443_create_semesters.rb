class CreateSemesters < ActiveRecord::Migration
  def change
    create_table :semesters do |t|
      t.integer :year, null: false
      t.integer :semester, null: false
    end

    add_index :semesters, [:year, :semester], unique: true
  end
end
