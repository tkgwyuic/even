class CreateGroupsInstructors < ActiveRecord::Migration
  def change
    create_join_table :groups, :instructors do |t|
      t.index :group_id
      t.index :instructor_id
    end
  end
end
