class CreateInstructorsSubjects < ActiveRecord::Migration
  def change
    create_join_table :instructors, :subjects
  end
end
