class CreateResults < ActiveRecord::Migration
  def change
    create_table :results do |t|
      t.references :students_subject, index: true, foreign_key: true, null: false

      t.integer :midterm
      t.integer :termend
      t.integer :resit
      t.integer :final

      t.integer :midterm_status, default: 0
      t.integer :termend_status, default: 0
      t.integer :resit_status, default: 0

      t.timestamps null: false
      t.timestamp :deleted_at
    end
  end
end
