class CreateGroups < ActiveRecord::Migration
  def change
    create_table :groups do |t|
      t.references :semester, index: true, foreign_key: true, null: false
      t.references :course, index: true, foreign_key: true, null: false

      t.string :name, null: false
      t.integer :grade, null: false

      t.timestamp :deleted_at
    end
  end
end
