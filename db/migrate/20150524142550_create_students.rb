class CreateStudents < ActiveRecord::Migration
  def change
    create_table :students do |t|
      t.string :code, null: false
      t.string :name, null: false
      t.string :ruby

      t.timestamps null: false

      t.timestamp :deleted_at
    end

    add_index :students, [:code, :deleted_at], unique: true
  end
end
