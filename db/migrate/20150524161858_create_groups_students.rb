class CreateGroupsStudents < ActiveRecord::Migration
  def change
    create_table :groups_students do |t|
      t.references :group, index: true, foreign_key: true
      t.references :student, index: true, foreign_key: true

      t.string :code
    end
  end
end
