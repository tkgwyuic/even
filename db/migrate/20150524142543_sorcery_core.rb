class SorceryCore < ActiveRecord::Migration
  def change
    create_table :instructors do |t|
      t.string :code, null: false
      t.string :name, null: false
      t.string :ruby, null: false

      t.string :crypted_password, null: false
      t.string :salt, null: false

      t.integer :perms, null: false, default: 0

      t.timestamps null: false
    end

    add_index :instructors, :code, unique: true
  end
end
