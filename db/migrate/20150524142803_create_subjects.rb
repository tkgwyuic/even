class CreateSubjects < ActiveRecord::Migration
  def change
    create_table :subjects do |t|
      t.references :semester, index: true, foreign_key: true, null: false
      t.references :base, index: true, foreign_key: true, null: false

      t.timestamp :deleted_at
    end
  end
end
